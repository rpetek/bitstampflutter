class CurrencyWrapper {
    CurrencyWrapper({this.currencies});

    List<Currency> currencies;

    factory CurrencyWrapper.fromJson(Map<String, dynamic> json) {
        return CurrencyWrapper(
            currencies: List.of(json["data"])
                .map((i) => Currency.fromJson(i))
                .toList()
        );
    }

}

class Currency {
    Currency({this.code, this.name, this.symbol, this.logo});

    String code;
    String name;
    String symbol;
    String logo;

    factory Currency.fromJson(Map<String, dynamic> json) {
        return Currency(
            code: json["code"],
            name: json["name"],
            symbol: json["symbol"],
            logo: json["logo"].substring(0, json["logo"].length - 4) + "@3x.png"
        );
    }

    @override
    String toString() {
        return 'Currency{code: $code, name: $name, symbol: $symbol, logo: $logo}';
    }


}