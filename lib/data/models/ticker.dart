class TickerWrapper {
    TickerWrapper({this.tickers});

    List<Ticker> tickers;

    factory TickerWrapper.fromJson(Map<String, dynamic> json) {
        return TickerWrapper(
            tickers: List.of(json["data"])
                .map((i) => Ticker.fromJson(i))
                .toList()
        );
    }

}

class Ticker {
    Ticker({this.base, this.counter, this.pair, this.high, this.low});

    String base;
    String counter;
    String pair;
    String high;
    String low;

    factory Ticker.fromJson(Map<String, dynamic> json) {
        return Ticker(
            base: json["pair"].substring(0, 3).toUpperCase(),
            counter: json["pair"].substring(3, 6).toUpperCase(),
            pair: json["pair"],
            high: json["high"],
            low: json["low"]
        );
    }

    @override
    String toString() {
        return 'Currency{base: $base, counter: $counter, pair: $pair, high: $high, low: $low}';
    }


}