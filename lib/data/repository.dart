import 'dart:async';

import 'package:bitstamp_flutter_app/data/models/currency.dart';

import 'api/rest_api.dart';
import 'models/ticker.dart';

class Repository {
    final apiProvider = RestApi();

    Future<List<Currency>> getCurrencies() => apiProvider.getCurrencies();

    Future<List<Ticker>> getTickers() => apiProvider.getTickers();
}