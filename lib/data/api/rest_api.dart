import 'dart:async';
import 'dart:convert';

import 'package:bitstamp_flutter_app/data/models/currency.dart';
import 'package:http/http.dart';

import '../models/ticker.dart';

class RestApi {
    Client client = Client();

    static final String baseUrl = "https://www.bitstamp.net/";
    static final String mobileApi = "api/mobile/v1/";
    static final String currencies = "currencies/";
    static final String tickers = "tickers/";
    final String currenciesUrl = "$baseUrl$mobileApi$currencies";
    final String tickersUrl = "$baseUrl$mobileApi$tickers";

    bool isSuccessful(Response response) {
        return response.statusCode >= 200 || response.statusCode < 400;
    }

    Future<List<Currency>> getCurrencies() async {
        final response = await client.get(currenciesUrl);
        print(response.body.toString());
        if (isSuccessful(response)) {
            CurrencyWrapper data = CurrencyWrapper.fromJson(
                json.decode(response.body));
            return data.currencies;
        } else {
            throw Exception('Failed to load currencies');
        }
    }

    Future<List<Ticker>> getTickers() async {
        final response = await client.get(tickersUrl);
        print(response.body.toString());
        if (isSuccessful(response)) {
            TickerWrapper data = TickerWrapper.fromJson(
                json.decode(response.body));
            return data.tickers;
        } else {
            throw Exception('Failed to load tickers');
        }
    }
}
