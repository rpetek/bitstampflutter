import 'package:bitstamp_flutter_app/screens/home/item_model.dart';
import 'package:flutter/material.dart';

import '../../colors.dart' as Color;

class ItemDetailPage extends StatefulWidget {
    ItemDetailPage({Key key, this.item}) : super(key: key);

    final ItemModel item;

    @override
    ItemDetailPageState createState() => ItemDetailPageState();
}

class ItemDetailPageState extends State<ItemDetailPage> {

    @override
    void initState() {
        super.initState();
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                // Here we take the value from the MyHomePage object that was created by
                // the App.build method, and use it to set our appbar title.
                title: Text(widget.item.name),
                backgroundColor: Color.accent,
            ),
            body: SafeArea(
                child: ListView(
                    children: <Widget>[
                        const SizedBox(height: 50.0),
                        Container(
                            padding: const EdgeInsets.only(bottom: 26),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment
                                    .center,
                                children: [
                                    Container(
                                        height: 36,
                                        child: Image.network(
                                            widget.item.logo),
                                    ),
                                    const SizedBox(height: 16.0),
                                    Column(
                                        children: [
                                            /*2*/
                                            Container(
                                                padding: const EdgeInsets
                                                    .only(
                                                    bottom: 8),
                                                child: Text(
                                                    widget.item.name,
                                                    style: TextStyle(
                                                        fontWeight: FontWeight
                                                            .bold,
                                                    ),
                                                ),
                                            ),
                                            Text(
                                                "${widget.item.symbol} ${widget
                                                    .item
                                                    .priceHigh}",
                                                style: TextStyle(
                                                    color: Colors.grey[500],
                                                ),
                                            ),
                                        ],
                                    ),
                                ],
                            ),
                        ),
                        const Divider(height: 1),
                        Padding(
                            padding: EdgeInsets.all(18),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceBetween,
                                children: [
                                    /*2*/
                                    Container(
                                        child: Text(
                                            'High',
                                            style: TextStyle(
                                                color: Colors.grey[500],
                                            ),
                                        ),
                                    ),
                                    Text(
                                        "${widget.item.symbol} ${widget.item
                                            .priceHigh}",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                        ),
                                    ),
                                ],
                            ),
                        ),
                        Padding(
                            padding: EdgeInsets.all(18),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceBetween,
                                children: [
                                    /*2*/
                                    Container(
                                        child: Text(
                                            'Low',
                                            style: TextStyle(
                                                color: Colors.grey[500],
                                            ),
                                        ),
                                    ),
                                    Text(
                                        "${widget.item.symbol} ${widget.item
                                            .priceLow}",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                        ),
                                    ),
                                ],
                            ),
                        ),
                    ],
                ),
            ),
        );
    }
}
