import 'dart:async';

import 'package:bitstamp_flutter_app/data/models/currency.dart';
import 'package:bitstamp_flutter_app/data/models/ticker.dart';
import 'package:bitstamp_flutter_app/data/repository.dart';
import 'package:bitstamp_flutter_app/screens/home/item_model.dart';

class HomeBloc {
    HomeBloc(this._repository);

    final Repository _repository;
    final _homeStreamController = StreamController<HomeState>();

    Stream<HomeState> get state => _homeStreamController.stream;

    void load() {
        _homeStreamController.sink.add(HomeState._loading());
        _repository.getCurrencies().then((currencies) {
            _repository.getTickers().then((tickers) {
                List<ItemModel> items = List();
                for (Currency currency in currencies) {
                    var item;
                    for (Ticker ticker in tickers) {
                        if (ticker.counter == "USD") {
                            item = ItemModel(
                                code: currency.code,
                                name: currency.name,
                                symbol: currency.symbol,
                                logo: currency.logo,
                                priceHigh: ticker.high,
                                priceLow: ticker.low
                            );
                        }
                    }
                    if (item != null) {
                        items.add(item);
                    }
                }
                _homeStreamController.sink.add(HomeState._data(items));
            });
        });
    }

    void dispose() {
        _homeStreamController.close();
    }
}

class HomeState {
    HomeState();

    factory HomeState._loading() = HomeLoadingState;

    factory HomeState._data(List<ItemModel> data) = HomeDataState;
}

class HomeLoadingState extends HomeState {}

class HomeDataState extends HomeState {
    HomeDataState(this.data);

    final List<ItemModel> data;
}