import 'package:bitstamp_flutter_app/colors.dart' as Color;
import 'package:bitstamp_flutter_app/data/repository.dart';
import 'package:bitstamp_flutter_app/screens/home/home_bloc.dart';
import 'package:bitstamp_flutter_app/screens/home/item_model.dart';
import 'package:bitstamp_flutter_app/screens/home/item_row.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
    HomeScreen({Key key, this.title}) : super(key: key);

    final String title;
    final Repository _repository = Repository();

    @override
    _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
    HomeBloc _homeBloc;

    final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey = GlobalKey<
        RefreshIndicatorState>();

    @override
    void initState() {
        super.initState();
        _homeBloc = HomeBloc(widget._repository);
        _homeBloc.load();
    }

    @override
    void dispose() {
        _homeBloc.dispose();
        super.dispose();
    }

    Future<void> _refresh() async {
        return _homeBloc.load();
    }

    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                backgroundColor: Color.accent,
                title: Text(widget.title),
            ),
            body: SafeArea(
                child: StreamBuilder<HomeState>(
                    stream: _homeBloc.state,
                    initialData: HomeLoadingState(),
                    builder: (context, snapshot) {
                        if (snapshot.data is HomeDataState) {
                            HomeDataState state = snapshot.data;
                            return _buildContent(state.data);
                        }
                        else if (snapshot.data is HomeLoadingState) {
                            try {
                                _refreshIndicatorKey.currentState.show();
                            } catch (NoSuchMethodError) {

                            }
                            return _buildLoading();
                        }
                        return Container();
                    },
                ),
            ),
        );
    }

    Widget _buildLoading() {
        return const Center(
            child: CircularProgressIndicator(),
        );
    }

    Widget _buildContent(List<ItemModel> data) {
        return RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: _refresh,
            child: ListView.builder(
                itemCount: data.length,
                itemBuilder: (BuildContext context, int index) {
                    return buildItemRowCurrency(
                        context, data[index]);
                })
        );
    }
}