class ItemModel {
    ItemModel(
        {this.code, this.name, this.symbol, this.logo, this.priceHigh, this.priceLow});

    String code;
    String name;
    String symbol;
    String logo;
    String priceHigh;
    String priceLow;

    @override
    String toString() {
        return 'Currency{code: $code, name: $name, symbol: $symbol, logo: $logo, priceHigh: $priceHigh, priceLow: $priceLow}';
    }

}