import 'package:bitstamp_flutter_app/screens/home/item_model.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../details/item_details.dart';

Widget buildItemRowCurrency(BuildContext context, ItemModel item) {
    return Material(
        color: Colors.white,
        child: InkWell(
            onTap: () {
                Navigator.push(context,
                    PageTransition(
                        type: PageTransitionType.rightToLeft,
                        duration: Duration(milliseconds: 250),
                        child: ItemDetailPage(item: item)
                    )
                );
            },
            child: Container(
                padding: const EdgeInsets.all(18),
                child: Row(
                    children: [
                        Padding(
                            padding: EdgeInsets.only(right: 18),
                            child: Container(
                                height: 36,
                                child: Image.network(item.logo)
                            )
                        ),
                        Expanded(
                            /*1*/
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    /*2*/
                                    Container(
                                        padding: const EdgeInsets.only(
                                            bottom: 8),
                                        child: Text(
                                            item.name,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                            ),
                                        ),
                                    ),
                                    Text(
                                        item.code,
                                        style: TextStyle(
                                            color: Colors.grey[500],
                                        ),
                                    ),
                                ],
                            ),
                        ),
                    ],
                ),
            ),
        ),
    );
}