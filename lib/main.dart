import 'package:flutter/material.dart';

import 'package:bitstamp_flutter_app/screens/home/home_screen.dart';

void main() => runApp(App());

class App extends StatelessWidget {
    // This widget is the root of your application.
    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            title: 'Bitstamp',
            theme: ThemeData(
                primarySwatch: Colors.blue,
            ),
            home: HomeScreen(title: 'Bitstamp'),
        );
    }
}
